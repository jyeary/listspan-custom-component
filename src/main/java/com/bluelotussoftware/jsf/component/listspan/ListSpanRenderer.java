/*
 * Copyright 2012-2013 Blue Lotus Software, LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * $Id$
 */
package com.bluelotussoftware.jsf.component.listspan;

import com.sun.faces.renderkit.Attribute;
import static com.sun.faces.renderkit.Attribute.*;
import com.sun.faces.renderkit.RenderKitUtils;
import com.sun.faces.renderkit.html_basic.HtmlBasicRenderer;
import java.io.IOException;
import java.util.Collection;
import java.util.logging.Level;
import javax.faces.FacesException;
import javax.faces.component.UIComponent;
import javax.faces.component.ValueHolder;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.convert.Converter;
import javax.faces.render.FacesRenderer;

/**
 *
 * @author John Yeary <jyeary@bluelotussoftware.com>
 * @version 1.0
 */
@FacesRenderer(componentFamily = "javax.faces.Output",
rendererType = "com.bluelotussoftware.component.ListSpan")
public class ListSpanRenderer extends HtmlBasicRenderer {

    Attribute[] attributes = new Attribute[]{
        attr("style")
    };

    public ListSpanRenderer() {
    }

    @Override
    public void encodeBegin(FacesContext context, UIComponent component) throws IOException {
        rendererParamsNotNull(context, component);
        ResponseWriter writer = context.getResponseWriter();
        writer.startElement("span", component);
        writeIdAttributeIfNecessary(context, writer, component);
        RenderKitUtils.renderPassThruAttributes(context, writer, component, attributes);
        Object obj = getValue(component);
        if (obj instanceof Collection<?>) {

            Collection<?> collection = (Collection<?>) obj;

            for (Object o : collection) {
                String value;

                if (o instanceof String) {
                    value = (String) o;
                } else {
                    Converter converter = ((ValueHolder) component).getConverter();

                    if (converter != null) {
                        value = converter.getAsString(context, component, o);
                    } else {
                        value = o.toString();
                    }
                }

                writer.startElement("span", component);

                if (component.getAttributes().get("elementStyle") != null) {
                    writer.writeAttribute("style", ((String) component.getAttributes().get("elementStyle")), "style");
                }

                writer.writeText(value, null);
                writer.endElement("span");
            }
        } else {
            throw new FacesException("The value must be a java.util.Collection");
        }
    }

    @Override
    public void encodeEnd(FacesContext context, UIComponent component) throws IOException {
        ResponseWriter writer = context.getResponseWriter();
        writer.endElement("span");
    }

    @Override
    protected Object getValue(UIComponent component) {
        if (component instanceof ValueHolder) {
            Object value = ((ValueHolder) component).getValue();
            if (logger.isLoggable(Level.FINE)) {
                logger.log(Level.FINE, "component.getValue() returned {0}", value);
            }
            return value;
        }
        return null;
    }

    @Override
    public boolean getRendersChildren() {
        return true;
    }
}